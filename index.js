import { Platform , AppRegistry} from 'react-native';
import { registerRootComponent } from 'expo';
import FlowsenseSDK from 'react-native-flowsense';

import App from './App';
import bgMessaging from './bgMessaging'

if (Platform.OS === 'ios') {
  FlowsenseSDK.startFlowsenseService("5779c6862a9c4b988003842ac6b84f42");
}
else if (Platform.OS === 'android') {
  FlowsenseSDK.startFlowsenseService("e445d39955f44b7a87d9627835672ba7");
}
// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);

// AppRegistry.registerComponent('FlowsenseSampleProjectReact', () => App);