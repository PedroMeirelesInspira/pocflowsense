import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import FlowsenseSDK from 'react-native-flowsense';
import firebase from "react-native-firebase";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default class App extends React.Component {
  componentWillMount() {
    FlowsenseSDK.inAppEventLocalized("teste", {});


    alert(Platform.OS);
    if (Platform.OS === 'ios') {
      FlowsenseSDK.updatePartnerUserID("Teste - Inspira - pocFlowsenseiOS");

    } else {
      FlowsenseSDK.updatePartnerUserID("Teste - Inspira - pocFlowsenseAnd");

    }

    // Obrigatório
    if (Platform.OS === 'ios' || Platform.OS === 'android') {
      FlowsenseSDK.requestAlwaysAuthorization();
    }

    FlowsenseSDK.startMonitoringLocation();

    // ask for notification permission
    firebase.messaging().requestPermission()
      .then(() => {
        firebase.messaging().getToken().then(result => {
          console.log('FCM Token: ' + result);
        });
      })
      .catch(error => {
        // User has rejected permissions
      });

    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          console.log("FCM Token: ", fcmToken);
        }
      })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }

}