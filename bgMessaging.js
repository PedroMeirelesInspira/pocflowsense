/**
 * No arquivo bgMessaging.js
 */
import type { RemoteMessage } from 'react-native-firebase';
import FlowsenseSDK from 'react-native-flowsense';


export default async (message: RemoteMessage) => {
  console.log("BgMessaging: Entered here");
  if (message.data.is_flowsense === true || message.data.is_flowsense === 'true') {
    FlowsenseSDK.sendMessageToFlowsense(message.data);
  }
  return Promise.resolve();
}

console.log("this file was loaded");