#ifndef RNFlowsenseSDK_h
#define RNFlowsenseSDK_h

#import <FlowsenseSDK/Service_fs.h>
#import <FlowsenseSDK/KeyValuesManager.h>
#import <FlowsenseSDK/InAppEvents.h>
#if __has_include(<React/RCTBridgeModule.h>)
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"
#import "React/RCTLog.h"
#else
#import "RCTBridgeModule.h"
#import "RCTEventEmitter.h"
#import "RCTLog.h"
#import "RCTViewManager.h"
#import "RCTEventDispatcher.h"
#endif

//#import <FlowsenseSDK/Push_fs.h>

@interface RNFlowsenseSDK : RCTEventEmitter<RCTBridgeModule>

+ (void)sendEventWithName:(NSString *)name withBody:(NSDictionary *)body;

@end

#endif /* FlowsenseSDK */

