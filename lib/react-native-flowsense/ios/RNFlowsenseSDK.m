#import "RNFlowsenseSDK.h"

@implementation RNFlowsenseSDK{
    
}

RCT_EXPORT_MODULE(FlowsenseSDK);

-(instancetype)init {
  if (self = [super init]) {
    
    for (NSString *eventName in [self supportedEvents])
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(emitEvent:) name:eventName object:nil];
  }
  
  return self;
}

- (void) startObserving {
  [[NSNotificationCenter defaultCenter] postNotificationName:@"bridgeSet" object:nil];
}

- (void)emitEvent:(NSNotification *)notification {
  [self sendEventWithName:notification.name body:notification.userInfo];
}

+ (void)sendEventWithName:(NSString *)name withBody:(NSDictionary *)body {
  [[NSNotificationCenter defaultCenter] postNotificationName:name object:nil userInfo:body];
}

// Uses main thread
- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

+(BOOL)requiresMainQueueSetup {
    return YES;
}

-(NSArray<NSString *> *)supportedEvents {
    return @[@"pushReceivedCallback", @"pushClickedCallback", @"pushTokenCallback", @"pushPermissionCallback", @"authorizationStatus"];
}

// Send events to Javascript
@synthesize bridge = _bridge;

RCT_EXPORT_METHOD(isAtHome:(RCTResponseSenderBlock)callback)
{
  callback(@[[NSNull null], [NSNumber numberWithBool:[Service_fs isInsideHome]]]);
}

RCT_EXPORT_METHOD(isAtWork:(RCTResponseSenderBlock)callback)
{
    callback(@[[NSNull null], [NSNumber numberWithBool:[Service_fs isInsideWork]]]);
}

RCT_EXPORT_METHOD(startFlowsenseService:(NSString *) partnerToken)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [Service_fs StartFlowsenseService:partnerToken :NO];
  });
}

RCT_EXPORT_METHOD(startMonitoringLocation)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [Service_fs StartMonitoringLocation];
  });
}

RCT_EXPORT_METHOD(stopFlowsenseService)
{
    [Service_fs StopFlowsenseService];
}

RCT_EXPORT_METHOD(requestLocationAuthorizationStatusAsync)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [Service_fs requestLocationAuthorizationStatusAsync:^(int status) {
        [self sendEventWithName:@"authorizationStatus" body:[NSNumber numberWithInt:status]];
    }];
  });
}

RCT_EXPORT_METHOD(requestWhenInUseAuthorizationWithCallback)
{
    dispatch_async(dispatch_get_main_queue(), ^{
      [Service_fs requestWhenInUseLocationWithCallback:^(int status) {
          [self sendEventWithName:@"authorizationStatus" body:[NSNumber numberWithInt:status]];
      }];
    });
}

RCT_EXPORT_METHOD(requestAlwaysAuthorizationWithCallback)
{
    dispatch_async(dispatch_get_main_queue(), ^{
      [Service_fs requestAlwaysLocationWithCallback:^(int status) {
          [self sendEventWithName:@"authorizationStatus" body:[NSNumber numberWithInt:status]];
      }];
    });
}

RCT_EXPORT_METHOD(requestWhenInUseAuthorization)
{
    dispatch_async(dispatch_get_main_queue(), ^{
      [Service_fs requestWhenInUseLocation];
    });
}

RCT_EXPORT_METHOD(requestAlwaysAuthorization)
{
    dispatch_async(dispatch_get_main_queue(), ^{
      [Service_fs requestAlwaysLocation];
    });
}

RCT_EXPORT_METHOD(updatePartnerUserID:(NSString *) puid)
{
    [Service_fs updatePartnerUserIdiOS:puid];
}

RCT_EXPORT_METHOD(setKeyValueString:(NSString *)key :(NSString *)value)
{
    [KeyValuesManager setKeyValue:key valueString:value];
}

RCT_EXPORT_METHOD(setKeyValueNumber:(NSString *)key :(nonnull NSNumber *)value)
{
    [KeyValuesManager setKeyValue:key valueDouble:[value doubleValue]];
}

RCT_EXPORT_METHOD(setKeyValueBoolean:(NSString *)key :(BOOL)value)
{
    [KeyValuesManager setKeyValue:key valueBoolean:value];
}

RCT_EXPORT_METHOD(setKeyValueDate:(NSString *)key :(nonnull NSNumber *)value)
{
    [KeyValuesManager setKeyValue:[key stringByReplacingOccurrencesOfString:@"FSDate_" withString:@""] valueDate:[NSDate dateWithTimeIntervalSince1970:[value longValue]/1000]];
}

RCT_EXPORT_METHOD(commitChanges)
{
    [KeyValuesManager commitChanges];
}

RCT_EXPORT_METHOD(inAppEvent:(NSString *)eventName :(NSDictionary *)map)
{
    [InAppEvents sendEventWithName:eventName values:map];
}

RCT_EXPORT_METHOD(inAppEventLocalized:(NSString *)eventName :(NSDictionary *)map)
{
    [InAppEvents sendGeolocalizedEventWithName:eventName values:map];
}

RCT_EXPORT_METHOD(sendPushToken:(NSString *)token)
{
    NSData *deviceToken = [token dataUsingEncoding:NSUTF8StringEncoding];
    [Service_fs sendPushToken:deviceToken];
}

RCT_EXPORT_METHOD(requestPushToken)
{
  dispatch_async(dispatch_get_main_queue(), ^{
    [Service_fs requestPushToken];
  });
}

//RCT_EXPORT_METHOD(sendMessageToFlowsense:(NSDictionary *)dict)
//{
//  [Push_fs flowsenseHandlePushPayload:dict];
//}
//

@end

