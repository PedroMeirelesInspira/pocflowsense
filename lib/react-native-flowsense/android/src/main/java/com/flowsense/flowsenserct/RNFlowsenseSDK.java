package com.flowsense.flowsenserct;

import android.*;
import android.app.Application;
import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.Nullable;

import android.util.Log;
import android.os.Handler;
import android.os.Bundle;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.modules.core.PermissionAwareActivity;
import com.facebook.react.modules.core.PermissionListener;
import com.flowsense.flowsensesdk.AppUsage.MonitorAppUsage;
import com.flowsense.flowsensesdk.FlowsenseSDK;
import com.flowsense.flowsensesdk.KeyValues.KeyValuesManager;
import com.flowsense.flowsensesdk.PushNotification.FlowsenseNotification;
import com.flowsense.flowsensesdk.PushNotification.FlowsensePushService;
import com.flowsense.flowsensesdk.PushNotification.FCM.FlowsenseHandlePush;
import com.flowsense.flowsensesdk.PushNotification.FCM.FlowsensePushToken;
import com.flowsense.flowsensesdk.Model.DeviceModel;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.facebook.react.bridge.Callback;


public class RNFlowsenseSDK extends ReactContextBaseJavaModule implements PermissionListener, LifecycleEventListener {

    private ReactApplicationContext reactApplicationContext;
    private static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 122;
    public static String RECEIVED_PUSH = "PUSH_RECEIVED";
    public static String CLICKED_PUSH = "PUSH_CLICKED";
    private int activityCount = 0;

    public RNFlowsenseSDK(ReactApplicationContext context) {
        super(context);
        reactApplicationContext = context;
        getReactApplicationContext().addLifecycleEventListener(this);
    }

    private void startFlowsense() {
        if (activityCount == 0) {
            if (FlowsensePushService.getInstance().getPushCallbacks() == null) {
                FlowsensePushService.getInstance().setPushCallback(new PushCallback(
                        getReactApplicationContext()));

                receivedNotifReceiver();
                clickedNotifReceiver();
            }
            activityCount++;
            if (reactApplicationContext != null && getCurrentActivity() != null) {
                try {
                    MonitorAppUsage.getInstance(
                            (Application) reactApplicationContext.getApplicationContext()).startFromActivity(getCurrentActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("FlowsenseSDK", e.toString());
                }

            }
        }
    }

    @ReactMethod
    public void isAtHome(Callback successCallback, Callback errorCallback) {
        try {
            successCallback.invoke(DeviceModel.getInstance(getReactApplicationContext()).isInsideHome());
        } catch (Exception e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void isAtWork(Callback successCallback, Callback errorCallback) {
        try {
            successCallback.invoke(DeviceModel.getInstance(getReactApplicationContext()).isInsideWork());
        } catch (Exception e) {
            errorCallback.invoke(e.getMessage());
        }
    }

    /**
     * Function used to start the SDK process
     *
     * @param partnerToken An Authorization token used across the SDK specially in HTTP headers
     */
    @ReactMethod
    public void startFlowsenseService(String partnerToken) {
        Context context = reactApplicationContext.getCurrentActivity();

        if (context == null) {
            context = reactApplicationContext.getApplicationContext();
        }
        FlowsenseSDK.init(partnerToken, context);
    }

    @ReactMethod
    public void startMonitoringLocation() {
        this.requestAuthAndStartLocation();
    }

    @ReactMethod
    public void setKeyValueString(String key, String value) {
        new KeyValuesManager(getReactApplicationContext()).setKeyValues(key, value);
    }

    @ReactMethod
    public void setKeyValueBoolean(String key, Boolean value) {
        new KeyValuesManager(getReactApplicationContext()).setKeyValues(key, value);
    }

    @ReactMethod
    public void setKeyValueNumber(String key, Double value) {
        try {
            new KeyValuesManager(getReactApplicationContext()).setKeyValues(key, value);
        } catch (Exception e) {
            Log.v("FlowsenseSDK", e.toString());
        }
    }

    @ReactMethod
    public void setKeyValueDate(String key, Double value) {
        try {
            long dateLong = value.longValue();
            Date date = new Date(dateLong);
            new KeyValuesManager(getReactApplicationContext()).setKeyValues(key, date);
        } catch (Exception e) {
            Log.v("FlowsenseSDK", e.toString());
        }
    }

    @ReactMethod
    public void commitChanges() {
        new KeyValuesManager(getReactApplicationContext()).commitChanges();
    }

    @ReactMethod
    public void updatePartnerUserID(String puid) {
        FlowsenseSDK.updatePartnerUserId(puid, getReactApplicationContext());
    }

    @ReactMethod
    public void inAppEvent(String eventName, ReadableMap map) {
        Map<String, Object> mMap = recursivelyDeconstructReadableMap(map);
        FlowsenseSDK.sendEvent(eventName, mMap, getReactApplicationContext());
    }

    @ReactMethod
    public void inAppEventLocalized(String eventName, ReadableMap map) {
        Map<String, Object> mMap = recursivelyDeconstructReadableMap(map);
        FlowsenseSDK.sendGeolocalizedEvent(eventName, mMap, getReactApplicationContext());
    }

    private Map<String, Object> recursivelyDeconstructReadableMap(ReadableMap readableMap) {
        ReadableMapKeySetIterator iterator = readableMap.keySetIterator();
        Map<String, Object> deconstructedMap = new HashMap<>();
        while (iterator.hasNextKey()) {
            String key = iterator.nextKey();
            ReadableType type = readableMap.getType(key);
            if (key.contains("FSDate_")) {
                if (type == ReadableType.Number) {
                    try {
                        deconstructedMap.put(key.replace("FSDate_", ""), new Date(Double.valueOf(
                                readableMap.getDouble(key)).longValue()));
                    } catch (Exception e) {
                        Log.v("FlowsenseSDK", "Could not build Date from Long");
                    }
                } else {
                    Log.v("FlowsenseSDK", "Long Date is not correct! Skipping key-value pair");
                }
                key = iterator.nextKey();
                type = readableMap.getType(key);
            }
            switch (type) {
                case Null:
                    deconstructedMap.put(key, null);
                    break;
                case Boolean:
                    deconstructedMap.put(key, readableMap.getBoolean(key));
                    break;
                case Number:
                    deconstructedMap.put(key, readableMap.getDouble(key));
                    break;
                case String:
                    deconstructedMap.put(key, readableMap.getString(key));
                    break;
                case Map:
                    deconstructedMap.put(key, recursivelyDeconstructReadableMap(readableMap.getMap(key)));
                    break;
                case Array:
                    deconstructedMap.put(key, recursivelyDeconstructReadableArray(readableMap.getArray(key)));
                    break;

                default:
                    throw new IllegalArgumentException("Could not convert object with key: " + key + ".");
            }

        }
        return deconstructedMap;
    }

    private List<Object> recursivelyDeconstructReadableArray(ReadableArray readableArray) {
        List<Object> deconstructedList = new ArrayList<>(readableArray.size());
        for (int i = 0; i < readableArray.size(); i++) {
            ReadableType indexType = readableArray.getType(i);
            switch (indexType) {
                case Null:
                    deconstructedList.add(i, null);
                    break;
                case Boolean:
                    deconstructedList.add(i, readableArray.getBoolean(i));
                    break;
                case Number:
                    deconstructedList.add(i, readableArray.getDouble(i));
                    break;
                case String:
                    deconstructedList.add(i, readableArray.getString(i));
                    break;
                case Map:
                    deconstructedList.add(i, recursivelyDeconstructReadableMap(readableArray.getMap(i)));
                    break;
                case Array:
                    deconstructedList.add(i, recursivelyDeconstructReadableArray(readableArray.getArray(i)));
                    break;
                default:
                    throw new IllegalArgumentException("Could not convert object at index " + i + ".");
            }
        }
        return deconstructedList;
    }

    @ReactMethod
    public void stopFlowsenseService() {
        FlowsenseSDK.stopFlowsenseService(getReactApplicationContext());
    }

    @ReactMethod
    public void sendPushToken(String token) {
        final String userToken = token;
        Handler mainHandler = new Handler(getReactApplicationContext().getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                new FlowsensePushToken(getReactApplicationContext(), userToken).execute();
            }
        };
        mainHandler.post(myRunnable);
    }

    @ReactMethod
    public void sendMessageToFlowsense(ReadableMap map) {
        new FlowsenseHandlePush(getReactApplicationContext(), toMap(map, false));
    }

    @Nullable
    private static WritableMap jsonToWritableMap(JSONObject jsonObject) {
        WritableMap writableMap = new WritableNativeMap();

        if (jsonObject == null) {
            return null;
        }


        Iterator<String> iterator = jsonObject.keys();
        if (!iterator.hasNext()) {
            return null;
        }

        while (iterator.hasNext()) {
            String key = iterator.next();

            try {
                Object value = jsonObject.get(key);

                if (value == null) {
                    writableMap.putNull(key);
                } else if (value instanceof Boolean) {
                    writableMap.putBoolean(key, (Boolean) value);
                } else if (value instanceof Integer) {
                    writableMap.putInt(key, (Integer) value);
                } else if (value instanceof Double) {
                    writableMap.putDouble(key, (Double) value);
                } else if (value instanceof String) {
                    writableMap.putString(key, (String) value);
                } else if (value instanceof JSONObject) {
                    writableMap.putMap(key, jsonToWritableMap((JSONObject) value));
                } else if (value instanceof JSONArray) {
                    writableMap.putArray(key, jsonToReact((JSONArray) value));
                }
            } catch (JSONException ex) {
                // Do nothing and fail silently
            }
        }

        return writableMap;
    }

    private static WritableArray jsonToReact(JSONArray jsonArray) throws JSONException {
        WritableArray writableArray = Arguments.createArray();
        for(int i=0; i < jsonArray.length(); i++) {
            Object value = jsonArray.get(i);
            if (value instanceof Float || value instanceof Double) {
                writableArray.pushDouble(jsonArray.getDouble(i));
            } else if (value instanceof Number) {
                writableArray.pushInt(jsonArray.getInt(i));
            } else if (value instanceof String) {
                writableArray.pushString(jsonArray.getString(i));
            } else if (value instanceof JSONObject) {
                writableArray.pushMap(jsonToWritableMap(jsonArray.getJSONObject(i)));
            } else if (value instanceof JSONArray){
                writableArray.pushArray(jsonToReact(jsonArray.getJSONArray(i)));
            } else if (value == JSONObject.NULL){
                writableArray.pushNull();
            }
        }
        return writableArray;
    }

    private static Map<String, String> toMap(ReadableMap readableMap, boolean recursion) {
        Map<String, String> map = new HashMap<>();
        ReadableMapKeySetIterator iterator = readableMap.keySetIterator();

        while (iterator.hasNextKey()) {
            String key = iterator.nextKey();
            ReadableType type = readableMap.getType(key);

            switch (type) {
                case Null:
                    map.put(key, "");
                    break;
                case Boolean:
                    map.put(key, String.valueOf(readableMap.getBoolean(key)));
                    break;
                case Number:
                    map.put(key, String.valueOf(readableMap.getDouble(key)));
                    break;
                case String:
                    if (recursion) map.put(key, "\"" + readableMap.getString(key) + "\"");
                    else map.put(key, readableMap.getString(key));
                    break;
                case Map:
                    map.put(key, toMap(readableMap.getMap(key), true).toString());
                    break;
                case Array:
                    break;
            }
        }
        return map;
    }

    private JSONObject bundleToJSON(Bundle bundle) {
        JSONObject json = new JSONObject();
        Set<String> keys = bundle.keySet();
        for (String key : keys) {
            try {
                if (bundle.get(key) instanceof Bundle) {
                    json.put(key, bundleToJSON((Bundle) bundle.get(key)));
                }
                json.put(key, bundle.get(key));
            } catch (JSONException e) {
                //Handle exception here
                Log.e("FlowsenseSDK", e.getMessage());
            }
        }
        return json;
    }

    @ReactMethod
    public void requestAlwaysAuthorization() {
        this.requestAuthAndStartLocation();
    }

    private void requestAuthAndStartLocation() {
        Context context = getReactApplicationContext().getBaseContext();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (context.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    try {
                        PermissionAwareActivity activity = getPermissionAwareActivity();
                        activity.requestPermissions(new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_ACCESS_FINE_LOCATION, this);
                    } catch (Exception e) {
                        Log.e("FlowsenseSDK", e.getMessage());
                    }
                }
            }
            else if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {            
                try {
                    PermissionAwareActivity activity = getPermissionAwareActivity();
                    activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_ACCESS_FINE_LOCATION, this);
                } catch (Exception e) {
                    Log.e("FlowsenseSDK", e.getMessage());
                }
            }
        } else {
            FlowsenseSDK.startMonitoringLocation(context);
        }
    }

    private PermissionAwareActivity getPermissionAwareActivity() {
        Activity activity = getCurrentActivity();
        if (activity == null) {
            throw new IllegalStateException("Tried to use permissions API while not attached to an " +
                    "Activity.");
        } else if (!(activity instanceof PermissionAwareActivity)) {
            throw new IllegalStateException("Tried to use permissions API but the host Activity doesn't" +
                    " implement PermissionAwareActivity.");
        }
        return (PermissionAwareActivity) activity;
    }

    private JSONObject flowsenseNotificationToJson(FlowsenseNotification notification) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("is_flowsense", true);
            jsonObject.put("title", notification.getTitle());
            jsonObject.put("small_message", notification.getSmallMessage());
            jsonObject.put("big_message", notification.getBigMessage());
            jsonObject.put("action", notification.getAction());
            jsonObject.put("push_image_icon_url", notification.getBigIconURL());
            jsonObject.put("push_image_url", notification.getBigPictureURL());
            JSONObject dataObj = new JSONObject();
            dataObj.put("push_uuid", notification.getPushUUID());
            dataObj.put("app_uri", notification.getAppURI());
            dataObj.put("intent_extras", notification.getIntentExtras().toString());

            JSONArray jsonArray = new JSONArray();
            for (Bundle b : notification.getActionButtons()) {
                jsonArray.put(bundleToJSON(b));
            }
            dataObj.put("actionButtons", jsonArray);
            jsonObject.put("data", dataObj);
        } catch (Exception e) {
            Log.e("FlowsenseSDK", e.getMessage());
        }
        return jsonObject;
    }

    private void receivedNotifReceiver() {
        IntentFilter intentFilter = new IntentFilter(RECEIVED_PUSH);
        getReactApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    FlowsenseNotification flowsenseNotification = (FlowsenseNotification)
                            intent.getSerializableExtra("notification");
                    JSONObject jsonObject = flowsenseNotificationToJson(flowsenseNotification);
                    getReactApplicationContext()
                            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                            .emit("pushReceivedCallback", jsonToWritableMap(jsonObject));
                } catch (Exception e) {
                    Log.e("FlowsenseSDK", e.getMessage());
                }
            }
        }, intentFilter);
    }

    private void clickedNotifReceiver() {
        IntentFilter intentFilter = new IntentFilter(CLICKED_PUSH);
        getReactApplicationContext().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    FlowsenseNotification flowsenseNotification = (FlowsenseNotification)
                            intent.getSerializableExtra("notification");
                    JSONObject jsonObject = flowsenseNotificationToJson(flowsenseNotification);
                    getReactApplicationContext()
                            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                            .emit("pushClickedCallback", jsonToWritableMap(jsonObject));
                } catch (Exception e) {
                    Log.e("FlowsenseSDK", e.getMessage());
                }
            }
        }, intentFilter);
    }

    @Override
    public String getName() {
        return "FlowsenseSDK";
    }

    @Override
    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    FlowsenseSDK.startMonitoringLocation(getReactApplicationContext().getBaseContext());
                }
            }
        }
        return false;
    }

    @Override
    public void onHostResume() {
        startFlowsense();
    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {

    }
}
