package host.exp.exponent;


import com.facebook.react.ReactPackage;

import java.util.Arrays;
import java.util.List;

import org.unimodules.core.interfaces.Package;

import expo.loaders.provider.interfaces.AppLoaderPackagesProviderInterface;
import host.exp.exponent.generated.BasePackageList;
import okhttp3.OkHttpClient;

import com.azendoo.reactnativesnackbar.SnackbarPackage;  // <- add this



import io.fabric.sdk.android.Fabric;
import com.crashlytics.android.Crashlytics;



// Needed for `react-native link`
// import com.facebook.react.ReactApplication;
import com.apsl.versionnumber.RNVersionNumberPackage;
import com.flowsense.flowsenserct.FlowsensePackage;
import com.azendoo.reactnativesnackbar.SnackbarPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage; // <-- Add this line
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage; // <-- Add this line
import io.invertase.firebase.config.RNFirebaseRemoteConfigPackage; // <-- Add this line
import com.dylanvann.fastimage.FastImageViewPackage;
import com.apsl.versionnumber.RNVersionNumberPackage;

public class MainApplication extends ExpoApplication implements AppLoaderPackagesProviderInterface<ReactPackage> {

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  // Needed for `react-native link`
  public List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
        // Add your own packages here!
        // TODO: add native modules!


          new FastImageViewPackage(),
          new SnackbarPackage(),
          new RNFirebasePackage(),
          new RNFirebaseAnalyticsPackage(),
          new RNFirebaseCrashlyticsPackage(), 
          new RNFirebaseMessagingPackage(), 
          new RNFirebaseNotificationsPackage(),
          new RNFirebaseRemoteConfigPackage(),
          new RNVersionNumberPackage(),
          new FlowsensePackage()
    );
  }

  public List getExpoPackages() {
    return new BasePackageList().getPackageList();
  }

  @Override
  public String gcmSenderId() {
    return getString(R.string.gcm_defaultSenderId);
  }

  @Override
   public void onCreate() {
       super.onCreate();
       Fabric.with(this, new Crashlytics());
   }

  public static OkHttpClient.Builder okHttpClientBuilder(OkHttpClient.Builder builder) {
    // Customize/override OkHttp client here
    return builder;
  }
}
